# Drawing Panel

# What is this application?

Basically, this application allows the user to draw and edit Bezier curves on a canvas.
This application uses **Java**, **Javafx**, and **MVC design pattern** to implement the user interface for its drawing panel.

## Goal of this project
- Create a user-friendly application that allows the user to draw Bezier curves with all kinds of different properties
- Construct a **javafx** application using MVC design pattern
- Practice with the theory guidelines of GUI development 

## Project requirements
-  gradle version "6.6"
-  openjdk version "11.0.9.1"
-  javafx
-  Groovy

## Run the application

This is a gradle project. Hence, as long as your current environment satisfy
the [project requirements](#project_requirements), you will be able to run
`gradle build` and `gradle run` to play the game.

---
## More about the application

### Menu
- File  (if the current drawing is not saved, then a prompt will show up for most of these menu items)
    - New: the user can press this menu item to clean the current drawing
    - Load: load a drawing previously saved
    - Save: save the current drawing (not image)
    - Quit: exit the application
- Help  
    - About: display the dialog box for my information

### Tool palette (the upper pane of the left tool bar)
The user can press [ESC] or the button again to exit the mode.

- Pen tool: 
   - This tool allows the user to enter drawing mode. 
   - Inside the drawing mode, the user can click on the canvas to draw curves (points will show up where the mouse is clicked). 

- Selection tool: 
   - This tool allows the user to enter selection mode.
   - Inside the selection mode, the user can select a line and drag its vertices or the control points of some vertices.
   - A selected curve will have its vertices displayed while the other curves will not.
   - When the user moved the mouse near a curve in this mode, the curve will have some visual indication to show that the curves are allowed to be selected. 
   - The property panel will display the property of current selected line.

- Point addition tool: 
   - The user need to select a line before entering this mode. 
   - This mode allows the user to click onto a selected curve and add vertices to the curve. 

- Point type tool: 
   - The user need to select a line before entering this mode. 
   - This mode allows the user to switch the type of a node (vertex) between sharp and smooth.

- Eraser tool: 
   - This tool allows the user to enter deleting mode. 
   - This tool allows the user to delete any curve. 
   - Pressing [DEL] when a curve is selected will also delete the curve.

- Point removal tool: 
   - The user need to select a line before entering this mode. 
   - This mode allows the user to remove any point on the selected curve. Note that, if a curve only has only one node (vertex) left, it will be deleted.

### Property palette (the lower pane of the left tool bar)

If a curve is selected, then these components will be describing the properties of this curve.  
On the other hand, the user can modify the property of the selected line with these components.  

If no curve is selected, then it represents the property of the next curve to be drawn.  
That is, the user can modify these properties in order to draw the next line with such properties.  

- color picker: this is a color picker for the curve(s)
- 4 types of line thickness
- 4 types of line styles

#### Node type
- A node (vertex) in a curve can be either smooth or sharp. Note that, the default type of all nodes (vertices) is smooth.
- In order to switch the type of a curve, the user should use the Point type tool on the tool palette.
- If a node (vertex) is smooth, it will be marked as a light blue circle (if the curve is selected) with at most two light blue circles (control points) nearby.
- If a node (vertex) is sharp, it will be marked as a light brown circle (if the curve is selected) with no other aid points.

### Other details
- If the drawing is unsaved, the title will have a star '*' at the end.
- Different tool might make the cursor into different shapes.
- If the mouse is near to an interactable node/curve, the cursor shape will usually change to indicate this.
- A status bar at the bottom indicates which mode the user is currently in.
- If the user pressed an disabled button, a notification will show up on the bottom. This notification will change due to the number that the user 
clicked on the button. Hence, the user should be able to notify it eventually.


### Source for resources:

Four pngs resided in `/src/main/resources`:  
- pen.png: https://uxwing.com/edit-pen-icon/  
- eraser.png: https://uxwing.com/eraser-icon/  
- select.png: https://uxwing.com/hover-click-icon/  
- angle.png: https://www.clipartmax.com/max/m2i8Z5A0i8N4b1G6/  
