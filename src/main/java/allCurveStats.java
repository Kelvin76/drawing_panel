import javafx.scene.Group;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

// this class is used to populate data to the canvas and extract data from it
public class allCurveStats implements Serializable {

    public List<CurveStats> allCurvesStats = new ArrayList<>();

    // store all the data for curves
    allCurveStats(List<Curve> curves){
        // get all the data from curves
        // turn all curves into curveState
        for(Curve curve : curves){
            CurveStats curveStats = curve.getCurveStat();
            allCurvesStats.add(curveStats);
        }
    }

    // deserialize everything back and returns the object back
    public List<Curve> getObject(Model model, Group root){
        List<Curve> curves = new ArrayList<>();

        // populate data into curves
        for(CurveStats curveStats : allCurvesStats){
            Curve newCurve = curveStats.getObject(model,root);
            curves.add(newCurve);
        }

        return curves;
    }
}
