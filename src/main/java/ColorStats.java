import java.io.Serializable;

import javafx.scene.paint.Color;

// This is for the purpose of saving and loading color property
public class ColorStats implements Serializable
{
    private double red;
    private double green;
    private double blue;
    private double opacity;
    public ColorStats(Color color)
    {
        this.red = color.getRed();
        this.green = color.getGreen();
        this.blue = color.getBlue();
        this.opacity = color.getOpacity();
    }
    public Color getObject()
    {
        return new Color(red, green, blue, opacity);
    }
}