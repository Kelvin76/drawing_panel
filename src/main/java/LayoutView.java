import javafx.geometry.Insets;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;

// LayoutView takes care of the whole structure of panes
public class LayoutView implements IView{

    // reference to the model
    private Model model;

    // All components
    // The layout
    BorderPane mainPane = new BorderPane();
    BorderPane lowerPane = new BorderPane();
    BorderPane lowerLeftPane = new BorderPane();

    PropertyView propertyV;
    ToolbarView toolBarV;

    MenubarView menuBarV;
    public static CanvasView canvasV;

    // some config
    Background c_bg = new Background(new BackgroundFill(Color.BEIGE, CornerRadii.EMPTY, Insets.EMPTY));
    Border c_bd = new Border(new BorderStroke(Color.GRAY, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(1)));

    LayoutView(Model model) {

        this.model = model;

        // create each view, and tell them about the model
        // the views will register themselves with the model
        this.menuBarV = new MenubarView(model);
        this.propertyV = new PropertyView(model);
        this.toolBarV = new ToolbarView(model);
        this.canvasV = new CanvasView(model);

        // initialize the layout
        this.lowerLeftPane.setTop(this.toolBarV);
        this.lowerLeftPane.setCenter(this.propertyV);

        this.lowerPane.setLeft(this.lowerLeftPane);       // toolbar and property are on the left
        this.lowerPane.setCenter(this.canvasV);            // canvasV is on the right

        this.mainPane.setTop(this.menuBarV);              // menuBarV is on the top
        this.mainPane.setCenter(this.lowerPane);          // insert the lower pane in the bottom

        // add the status bar
        StatusView statusV = new StatusView(model);
        this.mainPane.setBottom(statusV);

        statusV.setPrefHeight(20);

        // register with the model when we're ready to start receiving data
        model.addView(this);
    }


    // return the mainPane to the main function
    public BorderPane getPane(){
        return mainPane;
    }

    @Override
    public void updateView() {
        // set the left bar size according to the window size
        lowerLeftPane.setPrefWidth(model.getLeftBarWidth());
        toolBarV.setPrefHeight(model.getLeftBarHeight());
    }
}
