import javafx.scene.Group;

import java.io.Serializable;

// This is for the purpose of saving and loading
public class CoordStats implements Serializable {
    public double x = 0 ;
    public double y = 0;

    // stat of circle
    // these are constants
//    public double circleX;
//    public double circleY;
//    public double circleRadius;

    public CoordStats control1 = null;
    public CoordStats control2 = null;

    // Point property
    public boolean isSmooth;
    public boolean isShown;

    // store all the data for Coord
    CoordStats(){}

    // deserialize everything back and returns the object back
    public Coord getObject(Group root){
        // populate data into Coord
        return new Coord(root, this);
    }

    // returns a Coord with only x and y value
    public Coord getCoordinate(){
        return new Coord(this.x,this.y);
    }

}
