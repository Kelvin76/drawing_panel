import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;

import java.util.List;

public class CanvasView extends Pane implements IView{

    private final String deleteID = "DELETE";

    // reference to the model
    private Group canvas = new Group();

    // some config
    Background c_bg = new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY));
    Border c_bd = new Border(new BorderStroke(Color.GRAY, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(1)));

    CanvasView(Model model) {
        // configure the settings of this pane
        this.setBackground(c_bg);
        this.setBorder(c_bd);
        this.setWidth(200);

        // initialize the layout
        this.getChildren().add(canvas);

        // register with the model when we're ready to start receiving data
        model.addView(this);

        // tell the model about this mouse click
        this.setOnMouseClicked(MouseEvent-> {
            // System.out.println("Mouse Clicked on Canvas: (x,y):" + MouseEvent.getX() + "," + MouseEvent.getY());
            model.canvasNewMouseClick(new Coord(MouseEvent.getX(),MouseEvent.getY(), null));

        });

        // change the cursor type according to the mode
        this.setOnMouseEntered(MouseEvent->{
            model.setCursorType(true);
        });
        // clear the cursor
        this.setOnMouseExited(MouseEvent->{
            model.setCursorType(false);
        });

        // try out

//        CubicCurve c1 = new CubicCurve(100,100,300,100,100,100,300,100);
//        c1.setFill(null);
//        c1.setStrokeWidth(9);
//        c1.setStroke(Color.BLACK);
//
//        CubicCurve c11 = new CubicCurve(100,150,100,150,300,150,300,150);
//        c11.setFill(null);
//        c11.setStrokeWidth(9);
//        c11.setStroke(Color.BLACK);
//
//        CubicCurve c2 = new CubicCurve(100,300,300,300,100,300,300,300);
//        c2.setFill(null);
//        c2.setStrokeWidth(9);
//        c2.setStroke(Color.BLACK);
//
//        canvas.getChildren().addAll(c1,c2,c11);

//        Line lin1 = new Line(10, 10, 600, 10);
//
//        lin1.setStroke(Color.BROWN);
//        lin1.setStrokeWidth(1);
//        lin1.getStrokeDashArray().addAll(7d,4d);
//
//
//
//        canvas.getChildren().addAll(lin1,rec);
    }

    public void removeCanvas(){
        // remove everything from the group
        for(Node n : canvas.getChildren()){
            n.setVisible(false);
        }
        canvas.getChildren().clear();
    }

    public void loadCanvas(List<Curve> nodes){
        // remove everything from the group
        //canvas.getChildren().addAll(nodes);
    }

    public Group getCanvas(){
        return canvas;
    }


    @Override
    public void updateView() {
        // delete unused nodes
        if(canvas != null){
            List<Node> nodeList = canvas.getChildren();
            for(int i = 0; i < nodeList.size();++i){
                if(nodeList.get(i).getId() == deleteID){
                    nodeList.remove(i);
                    i--;
                }
            }
        }
    }
}
