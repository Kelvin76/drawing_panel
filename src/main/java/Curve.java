import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.*;

import java.util.ArrayList;

public class Curve{

    // some constants
    final private double percentageForControlPt = 0.3;
    final private double strokeWidthAugment = 7;

    // store all the segments of cubic curves
    private ArrayList<CubicCurve> segments = new ArrayList<>();
    // list of points on the curve
    private ArrayList<Coord> points = new ArrayList<>();

    private Coord StartPoint;
    private Coord EndPoint = null;
    transient private Rectangle bound;
    final private static double boundOffset = 10;

    // Properties of the curve
    private Color color;
    private double thickness;
    private int pattern;
    // there is no fill
    final private Color fill = null;

    private boolean selected = true;

    // Canvas reference
    transient private Group root = null;
    transient private Model model;

    // for events
    double initialX, initialY;
    boolean hovered = false;

    // set the starting coordination
    Curve(Model model, Group root, Coord coord, Color color, double thickness, int pattern){
        // initialize
        // ignore bound
        bound = new Rectangle(0,0,0,0);
        bound.setVisible(false);

        this.model = model;
        this.setRoot(root);
        this.StartPoint = new Coord(coord,root);

        // properties
        this.color = color;
        this.thickness = thickness;
        this.pattern=pattern;

        points.add(StartPoint);

        // set events
        addListenerToPt(StartPoint);

        // if this graph is clicked, display the rectangle in dotted lines
    }

    /* ---------  Public  --------  */
    // turn on the visual indications
    public void turnOnVisualIndication(){
        // turn on all the points
        for(Coord p : points){
            p.turnOnDisplay();
        }
        // turn on the indication
        turnOnBound();
        // move all the segments to the front
        for(CubicCurve seg : segments){
            seg.toFront();
        }
    }

    public void turnOffVisualIndication(){
        // turn off all the points
        for(Coord p : points){
            p.turnOffDisplay();
        }
        // turn off the indication
        turnOffBound();
    }

    // indicate this line when hovered
    public void onHover(){
        hovered = true;
        for(CubicCurve seg : segments){
            if(color == Color.STEELBLUE){
                seg.setStroke(Color.TEAL);
            }else{
                seg.setStroke(Color.STEELBLUE);
            }
            seg.setStrokeWidth(thickness + strokeWidthAugment);
            seg.toFront();
        }
    }
    public void overHover(){
        if(hovered){
            for(CubicCurve seg : segments){
                seg.setStroke(color);
                seg.setStrokeWidth(thickness);
                seg.toFront();
            }
        }
        hovered = false;
    }


    /* ---------  Accessors  --------  */

    public Color getColor(){
        return color;
    }
    public double getThickness(){
        return thickness;
    }
    public int getPattern(){
        return pattern;
    }
    public ArrayList<CubicCurve> getSegments(){
        return segments;
    }
    public void setSegments(ArrayList<CubicCurve> segments){
        this.segments = segments;
    }

    public ArrayList<Coord> getPoints(){
        return points;
    }
    public void setPoints(ArrayList<Coord> points){
        this.points = points;
    }

    /* ---------  Mutators  --------  */

    public void addSegment(Coord coord){
        CubicCurve newCurve;
        ArrayList<Coord> controls;

        Coord prevPoint = EndPoint;

        if(EndPoint == null){
            // adding the second point
            prevPoint = StartPoint;
        }

        // adding segment
        // get the control points
        controls = calculateControlPoint(prevPoint,coord);
        // EndPoint.setControl2(controls.get(0));
        setControlPt(prevPoint,false,controls.get(0));
        // display the Coord
        prevPoint.turnOnDisplay();

        // add and set the segment
        newCurve = new CubicCurve(prevPoint.getX(),prevPoint.getY(),
                controls.get(0).getX(),controls.get(0).getY(),controls.get(1).getX(),controls.get(1).getY(),
                coord.getX(),coord.getY());

        // create new point
        EndPoint = new Coord(coord,root);
        // EndPoint.setControl1(controls.get(1));
        setControlPt(EndPoint,true,controls.get(1));
        // display the point
        EndPoint.turnOnDisplay();

        // add the new point and seg to the structure
        points.add(EndPoint);
        newCurve.setFill(fill);
        segments.add(newCurve);

        // modify the control points of the last point
        if(segments.size()>1){
            modifyControlPts(points.size() - 2);
        }

        // add listeners to the points and segments
        addListenerToPt(EndPoint);
        addListenerToSeg(newCurve);

        // add the point and segment onto the canvas
        this.addSegmentToRoot(newCurve);

        // tell the model that the drawing is changed
        model.unsaved();

        updateAll();
    }

    public void setColor(Color color){
        this.color = color;
        this.updateAll();
    }
    public void setThickness(double thickness){
        this.thickness = thickness;
        this.updateAll();
    }
    public void setPattern(int pattern){
        this.pattern = pattern;
        this.updateAll();
    }

    public void setRoot(Group root){
        if(this.root == null){
            this.root = root;
            this.pushItemsIntoGroup();
        }else{
            //System.out.println("Root already exists.");
        }
    }

    // select it to turn on visual indication
    public void selectIt(){
        this.selected = true;
        // need to tell the model about the line's properties
        model.setLineColor(color);
        model.setLineThickness(thickness);
        model.setLinePattern(pattern);
        turnOnVisualIndication();
    }

    // unselect it to turn off visual indication
    public void unselectIt(){
        // System.out.println("unselect this");
        this.selected = false;
        turnOffVisualIndication();
    }

    // a valid curve must have both start and end point being not null
    public boolean validCurve(){
        return (StartPoint != null && EndPoint != null);
    }

    /* ---------  Helper  --------  */
    // add everything into the root
    private void pushItemsIntoGroup(){
        for(CubicCurve curve : segments){
            addSegmentToRoot(curve);
        }
        root.getChildren().add(bound);
        bound.toBack();
    }
    // add 1 segment into the group
    private void addSegmentToRoot(CubicCurve seg){
        root.getChildren().add(seg);
        seg.toFront();
    }

    // add listeners to newly created point
    private void addListenerToPt(Coord point){
        // display the circle in the front
        point.getCircle().setOnMouseEntered(MouseEvent->{
            point.getCircle().toFront();
            if(model == null){
                return;
            }
            switch (model.getMode()){
                case SELECTING:
                case POINTTYPECHANGING:
                case REMOVING:
                    point.getCircle().setCursor(Cursor.HAND);
                    break;
                case ADDING:
                default:
                    point.getCircle().setCursor(Cursor.DEFAULT);
            }
            MouseEvent.consume();
        });
        // display the circle in the back when mouse is moved away
        point.getCircle().setOnMouseExited(new EventHandler<>(){
            public void handle(MouseEvent event) {
                point.getCircle().toBack();
            }
        });

        // change the point type
        point.getCircle().setOnMouseClicked(MouseEvent-> {
            // System.out.println("Clicked on Circle.");
            // need to be able to toggle the whole Curve
            model.lineSelected(this);
            if(model == null){
                return;
            }
            /* --------- POINT TYPE CHANGING ---------------- */
            switch (model.getMode()){
                case POINTTYPECHANGING:
                    CubicCurve seg1 = null;
                    CubicCurve seg2 = null;

                    if(point == StartPoint) {
                        seg2 = segments.get(0);
                    }else{
                        // find the corresponding segment
                        for(int i = 0; i < points.size(); ++i){
                            if(points.get(i) == point){
                                // need to be > 0 to relate to a segment before it
                                if(i > 0){
                                    seg1 = segments.get(i-1);
                                }
                                // need to be not the last point to have a segment after it
                                if(i < points.size()-1){
                                    seg2 = segments.get(i);
                                }
                                break;
                            }
                        }
                    }
                    // toggle the state
                    boolean isSmooth = point.toggleType();
                    // it was sharp
                    if(isSmooth){
                        // need to generate two new control points
                        // set the control points to its control points
                        for(int i = 0; i <points.size();++i){
                            if(points.get(i) == point){
                                modifyControlPts(i);
                                break;
                            }
                        }
                    }
                    // it was smooth
                    else{
                        // set the control points to itself
                        if(seg1 != null){
                            setControlPt(point,true,point);
                            seg1.setControlX2(point.getX());
                            seg1.setControlY2(point.getY());
//                            System.out.println("Point:"+point);
//                            System.out.println("Point control1:"+point.getControl1());
//                            System.out.println("Point control2:"+point.getControl2());
                        }
                        if(seg2 != null){
                            setControlPt(point,false,point);
                            seg2.setControlX1(point.getX());
                            seg2.setControlY1(point.getY());
                        }
                    }
                    // tell the model that the drawing is changed
                    model.unsaved();
                    break;
                /* -------------  Delete the point  -------------- */
                case REMOVING:
                    // DELETING POINT (REMOVING POINT) Logic
                    // first get the index of the point
                    int index = 0;
                    for(; index < points.size(); ++index){
                        if(points.get(index) == point){
                            break;
                        }
                    }

                    // get he previous segment and later segment
                    CubicCurve prevSeg = null;
                    CubicCurve nxtSeg = null;

                    if(index > 0){
                        prevSeg = segments.get(index - 1);
                    }
                    if(index < segments.size()){
                        nxtSeg = segments.get(index);
                    }

                    // if this is the Start point, we delete the point and the nxt segment
                    if(point == StartPoint){
                        // if only has two points, we can delete the curve
                        if(points.size() < 3){
                            this.deleteObject();
                            //model.deleteCurve(this);
                        }else{
                            // delete
                            deleteSegPoint(index, index);

                            StartPoint = points.get(0);
                            // also need to delete the first control point of next point
                            StartPoint.deleteControlPoint(true);
                        }
                    }
                    // if this is the End point, we delete the point and the prev segment
                    else if(point == EndPoint){
                        // if only has two points, we can delete the curve
                        if(points.size() < 3){
                            this.deleteObject();
                        }else{
                            // delete
                            deleteSegPoint(index - 1, index);

                            EndPoint = points.get(points.size()-1);
                            // also need to delete the second control point of previous point
                            EndPoint.deleteControlPoint(false);
                        }
                    }
                    // otherwise, we delete the previous segment and link the segments
                    else{

                        nxtSeg.setStartX(prevSeg.getStartX());
                        nxtSeg.setStartY(prevSeg.getStartY());
                        nxtSeg.setControlX1(prevSeg.getControlX1());
                        nxtSeg.setControlY1(prevSeg.getControlY1());

                        // delete
                        deleteSegPoint(index - 1, index);
                    }
                    // tell the model that the drawing is changed
                    if(model != null){
                        model.unsaved();
                    }
                    updateAll();
                default:
            }
        });

        /* -------------  Dragging the point  -------------- */
        // need to be able to move the point
        // this is the drag logic
        point.getCircle().setOnMousePressed(MouseEvent-> {
            // record the initial point
            initialX = point.getX();
            initialY = point.getY();
        });

        point.getCircle().setOnMouseDragged(MouseEvent-> {
            if(model != null && model.getMode() != Model.STATE.SELECTING){
                return;
            }

            CubicCurve seg1 = null;
            CubicCurve seg2 = null;

            if(point == StartPoint){
                seg2 = segments.get(0);
            }else{
                // find the corresponding segment
                for(int i = 0; i < points.size(); ++i){
                    if(points.get(i) == point){
                        // need to be > 0 to relate to a segment before it
                        if(i > 0){
                            seg1 = segments.get(i-1);
                        }
                        // need to be not the last point to have a segment after it
                        if(i < points.size()-1){
                            seg2 = segments.get(i);
                        }
                        break;
                    }
                }
            }

            double finalX = Math.max(0,MouseEvent.getX());
            double finalY = Math.max(0,MouseEvent.getY());

            // set the end point of front segment
            if(seg1 != null){
                seg1.setEndX(finalX);
                seg1.setEndY(finalY);
            }

            // set the start point of later segment
            if(seg2 != null){
                seg2.setStartX(finalX);
                seg2.setStartY(finalY);
            }

            // calculate the distance
            double xdiff = point.getX()-finalX;
            double ydiff = point.getY()-finalY;

            // set the point to the new position
            point.setX(finalX);
            point.setY(finalY);

            // set the control points to newer position
            // if this point is sharp, just set control to itself
            if(!point.isSmooth){
                if(point.getControl1() != null){
                    point.setControl1(point);
                }
                if(point.getControl2() != null){
                    point.setControl2(point);
                }
            }else{
                if(point.getControl1() != null){
                    point.setControl1(new Coord(Math.max(0,point.getControl1().getX()-xdiff),Math.max(0,point.getControl1().getY()-ydiff)));
                }
                if(point.getControl2() != null){
                    point.setControl2(new Coord(Math.max(0,point.getControl2().getX()-xdiff),Math.max(0,point.getControl2().getY()-ydiff)));
                }
            }

            // set the second control point of front segment
            if(seg1 != null){
                seg1.setControlX2(point.getControl1().getX());
                seg1.setControlY2(point.getControl1().getY());
            }
            // mark
            // set the first control point of later segment
            if(seg2 != null){
                seg2.setControlX1(point.getControl2().getX());
                seg2.setControlY1(point.getControl2().getY());
            }
            // tell the model that the drawing is changed
            updateAll();
            model.unsaved();
        });

    }

    // delete the segment and point for the indices (did not check boundary)
    private void deleteSegPoint(int segIndex, int pointIndex){
        // delete
        // turn it off
        segments.get(segIndex).setVisible(false);
        points.get(pointIndex).deleteThis();

        // remove from the canvas
        root.getChildren().remove(segments.get(segIndex));
        root.getChildren().remove(points.get(pointIndex));

        // remove from the structure
        segments.set(segIndex, null);
        points.set(pointIndex,null);

        segments.remove(segIndex);
        points.remove(pointIndex);
    }

    // add listeners to newly created curve
    private void addListenerToSeg(CubicCurve seg){
        // need to be able to toggle the whole Curve
        // if in select mode
        seg.setOnMouseClicked(MouseEvent->{
            // tell model, a new line is selected
            if(model == null){
                return;
            }
            model.lineSelected(this);
            // or may need to create a new node

            // ADDING POINT Logic
            // that is, adding point to the curve
            if(model != null && model.getMode() == Model.STATE.ADDING && model.getCurrentSelectedCurve() == this){
                // need to select current curve to work
                // find the index of this segment
                int index = 0;
                for(; index < segments.size(); ++index){
                    if(segments.get(index) == seg){
                        break;
                    }
                }

                // create 1 new point
                Coord newPoint = new Coord(MouseEvent.getX(),MouseEvent.getY(),root);
                // store the two adjacent points
                Coord prePt = points.get(index);
                Coord nxtPt = points.get(index+1);

                // create 1 new segments
                CubicCurve newSeg = new CubicCurve(prePt.getX(),prePt.getY(),prePt.getControl2().getX(), prePt.getControl2().getY(),
                        0,0,newPoint.getX(),newPoint.getY());
                // the original segment is used as the second segment
                seg.setStartX(newPoint.getX());
                seg.setStartY(newPoint.getY());
                seg.setControlX2(nxtPt.getControl1().getX());
                seg.setControlY2(nxtPt.getControl1().getY());
                seg.setEndX(nxtPt.getX());
                seg.setEndY(nxtPt.getY());

                // add the point and segs into the list
                points.add(index+1,newPoint);
                newSeg.setFill(fill);
                segments.add(index,newSeg);

                // create the control points
                modifyControlPts(index + 1);

                // add listeners
                addListenerToPt(newPoint);
                addListenerToSeg(newSeg);

                // add the segment onto the canvas
                this.addSegmentToRoot(newSeg);
                // tell the model that the drawing is changed
                model.unsaved();
                updateAll();
            }
        });
        // show effects
        seg.setOnMouseMoved(MouseEvent->{
            model.lineHovered(seg,this);
        });

        // display the circle in the front
        seg.setOnMouseEntered(MouseEvent->{
            if(model == null){
                return;
            }
            switch (model.getMode()){
                case SELECTING:
                case ERASING:
                    seg.setCursor(Cursor.HAND);
                    this.onHover();
                    break;
                case ADDING:
                    if(model.getCurrentSelectedCurve() == this){
                        seg.setCursor(Cursor.HAND);
                        this.onHover();
                    }else{
                        seg.setCursor(Cursor.DEFAULT);
                    }
                    break;
                case REMOVING:
                default:
                    seg.setCursor(Cursor.DEFAULT);
                    this.overHover();
            }
        });

        // mouse is over
        seg.setOnMouseExited(MouseEvent->{
            this.overHover();
        });

    }

    private void updateAll(){
        if(root == null){
            return;
        }
        // update the curves
        for(CubicCurve curve : this.segments){
            curve.setStrokeWidth(this.thickness);
            curve.setStroke(this.color);
            //System.out.println("Change pattern to "+pattern);
            setPattern(curve,pattern);
        }
        // update the points
        for(Coord point: points){
            point.setCircleRadius(thickness);
        }
        if(selected){
            // need to generate the new bound
            turnOnVisualIndication();
        }
    }

    // set patterns
    public void setPattern(CubicCurve curve, int pattern){
        switch(pattern){
            case 1:
                cleanDashedArray(curve);
                curve.getStrokeDashArray().addAll(15d,25d);
                break;
            case 2:
                cleanDashedArray(curve);
                curve.getStrokeDashArray().addAll(10d);
                break;
            case 3:
                cleanDashedArray(curve);
                curve.getStrokeDashArray().addAll(30d,15d,10d,15d);
                break;
            case 0:
            default:
                cleanDashedArray(curve);
        }
    }

    private void cleanDashedArray(CubicCurve curve){
        curve.getStrokeDashArray().clear();
    }

    // calculate and return two control points based on start point and end point
    private ArrayList<Coord> calculateControlPoint(Coord start, Coord end){
        ArrayList<Coord> result = new ArrayList<>();

        Coord r1 = new Coord();
        Coord r2 = new Coord();

        double x1,x2,y1,y2,xdiff,ydiff, xoffset,yoffset;
        x1 = start.getX();
        x2 = end.getX();
        y1 = start.getY();
        y2 = end.getY();

        xdiff = Math.abs(x1-x2);
        ydiff = Math.abs(y1-y2);

        xoffset = percentageForControlPt * xdiff;
        //System.out.println("Xoffset: " + xoffset);
        yoffset = percentageForControlPt * ydiff;
        //System.out.println("Yoffset: " + yoffset);

        if(xdiff == 0 || ydiff == 0){
            if(xdiff == 0){
                if(x1 > xoffset + 5){
                    // set x coordinate on the left
                    r1.setX(x1-xoffset);
                    r2.setX(x2-xoffset);
                }else{
                    // set x coordinate on the right
                    r1.setX(x1+xoffset);
                    r2.setX(x2+xoffset);
                }
                // add of subtract y correspondingly
                if(y1 > y2){
                    r1.setY(y1-yoffset);
                    r1.setY(y2+yoffset);
                }else{
                    r1.setY(y1+yoffset);
                    r1.setY(y2-yoffset);
                }
            }else{
                if(y1 > yoffset + 5){
                    // set Y coordinate on the top
                    r1.setY(y1-xoffset);
                    r2.setY(y2-xoffset);
                }else{
                    // set Y coordinate on the bottom
                    r1.setY(y1+xoffset);
                    r2.setY(y2+xoffset);
                }
                // add of subtract x correspondingly
                if(x1 > x2){
                    r1.setX(x1-yoffset);
                    r1.setX(x2+yoffset);
                }else{
                    r1.setX(x1+yoffset);
                    r1.setX(x2-yoffset);
                }
            }
        }else{
            if(x1 < x2){
                r1.setX(x1+xoffset);
                r2.setX(x2-xoffset);
            }else{
                r1.setX(x1-xoffset);
                r2.setX(x2+xoffset);
            }
            if(y1 < y2){
                r1.setY(y1+xoffset);
                r2.setY(y2-xoffset);
            }else{
                r1.setY(y1-xoffset);
                r2.setY(y2+xoffset);
            }
        }

        // check boundary
        r1.setX((r1.getX()<0)?0:r1.getX());
        r1.setY((r1.getY()<0)?0:r1.getY());
        r2.setX((r2.getX()<0)?0:r2.getX());
        r2.setY((r2.getY()<0)?0:r2.getY());

        result.add(r1);
        result.add(r2);
        //System.out.println("Calculate coordinates: " + result);
        return result;
    }

    // turn on the bound indication
    // basically we create this rectangle here (lots of room to improve the efficiency, but just implement it first)
    private void turnOnBound(){
        bound.setVisible(true);
        // initialize the boundary
        bound.setFill(fill);
        bound.setStrokeWidth(1);
        // clear the dashed array
        if(bound.getStrokeDashArray() != null){
            bound.getStrokeDashArray().clear();
        }
        bound.getStrokeDashArray().addAll(1d,4d);
        bound.setStrokeLineCap(StrokeLineCap.ROUND);
        bound.setStroke(Color.BLACK);

        // setting up the coordinates
        // basically we find the closest point to the origin and the one furthest from origin
        // we need to find two pairs, one for x coordinate, one for y coordinate
        double xMin = StartPoint.getX(),xMax = StartPoint.getX(),yMin = StartPoint.getY(),yMax = StartPoint.getY();

        for(Coord pt : points){
            if(pt.getX() < xMin){
                xMin = pt.getX();
            }else if (pt.getX() > xMax){
                xMax = pt.getX();
            }
            if(pt.getY() < yMin){
                yMin = pt.getY();
            }else if (pt.getY() > yMax){
                yMax = pt.getY();
            }
        }

        // reset xMin and yMin due to the offset
        xMin = Math.max(0,xMin - boundOffset);
        yMin = Math.max(0,yMin - boundOffset);
        xMax = xMax+boundOffset;
        yMax = yMax+boundOffset;

        // set the bound
        bound.setX(xMin);
        bound.setY(yMin);
        bound.setWidth(xMax-xMin);
        bound.setHeight(yMax-yMin);

        // set all other children of the root to the front
        if(root != null){
            bound.toBack();
        }
    }

    // turn off the bound indication
    // basically we just set it to invisible
    private void turnOffBound(){
        bound.setVisible(false);
    }



    // a helper function to set one point's control point
    /**
     *
     * @param isOne indicates if we want to modify the first control point or the second
     * @param point is the point to modify
     * @param coord the target location of the control point
     */
    private void setControlPt(Coord point,boolean isOne,Coord coord){
        // set the correct control point
        // if the point didn't exist, then we add listener to it
        Coord cPoint;
        Line cLine;
        // flag indicates which control point to alter
        int flag = 0;

        if(isOne){
            if(!point.setControl1(coord)){
                flag = 1;
            }
        }else{
            if(!point.setControl2(coord)){
                flag = 2;
            }
        }

        if(flag > 0 && flag < 3){
            // add some common event listeners
            if(flag == 1) {
                cPoint = point.getControl1();
                cLine = point.getLine1();
            }else{
                cPoint = point.getControl2();
                cLine = point.getLine2();
            }

            // display the circle in the front
            cPoint.getCircle().setOnMouseEntered(new EventHandler<>(){
                public void handle(MouseEvent event) {
                    cPoint.getCircle().toFront();
                    if(model == null){
                        return;
                    }
                    switch (model.getMode()){
                        case SELECTING:
                            cPoint.getCircle().setCursor(Cursor.HAND);
                            break;
                        case ADDING:
                        default:
                            point.getCircle().setCursor(Cursor.DEFAULT);
                    }
                }
            });
            // display the circle in the back when mouse is moved away
            cPoint.getCircle().setOnMouseExited(new EventHandler<>(){
                public void handle(MouseEvent event) {
                    cPoint.getCircle().toBack();
                }
            });

            cPoint.getCircle().setOnMousePressed(MouseEvent-> {
                // record the initial point
                initialX = cPoint.getX();
                initialY = cPoint.getY();
            });

            if(flag == 1){
                /* -------------  Dragging Control point  -------------- */
                cPoint.getCircle().setOnMouseDragged(MouseEvent-> {
                    // only selecting mode works
                    if(model != null && model.getMode() != Model.STATE.SELECTING){
                        return;
                    }
                    // flag == 1, deals with previous seg
                    CubicCurve prevSeg = null;
                    for(int i = 1; i < points.size(); ++i){
                        if(points.get(i) == point){
                            prevSeg = segments.get(i-1);
                            break;
                        }
                    }
                    if(prevSeg == null){
                        return;
                    }
                    // check the boundary
                    double finalX = Math.max(0,MouseEvent.getX());
                    double finalY = Math.max(0,MouseEvent.getY());
                    // set the end point of front segment
                    prevSeg.setControlX2(finalX);
                    prevSeg.setControlY2(finalY);
                    // set the control point to the new position
                    cPoint.setX(finalX);
                    cPoint.setY(finalY);
                    // change the line starting point
                    cLine.setStartX(finalX);
                    cLine.setStartY(finalY);
                });
                // tell the model that the drawing is changed
                model.unsaved();
            }else if(flag == 2){
                /* -------------  Dragging Control point  -------------- */
                cPoint.getCircle().setOnMouseDragged(MouseEvent-> {
                    // only selecting mode works
                    if(model != null && model.getMode() != Model.STATE.SELECTING){
                        return;
                    }
                    // flag == 2, deals with next seg
                    CubicCurve nxtSeg = null;
                    for(int i = 0; i < points.size() - 1; ++i){
                        if(points.get(i) == point){
                            nxtSeg = segments.get(i);
                            break;
                        }
                    }
                    if(nxtSeg == null){
                        return;
                    }
                    // check the boundary
                    double finalX = Math.max(0,MouseEvent.getX());
                    double finalY = Math.max(0,MouseEvent.getY());
                    // set the start point of later segment
                    nxtSeg.setControlX1(finalX);
                    nxtSeg.setControlY1(finalY);
                    // set the control point to the new position
                    cPoint.setX(finalX);
                    cPoint.setY(finalY);
                    // change the line starting point
                    cLine.setStartX(finalX);
                    cLine.setStartY(finalY);
                });
                // tell the model that the drawing is changed
                model.unsaved();
            }
        }
    }

    // modify both control points
    // the purpose of this function is to set two nearby points aligned with the point
    private void modifyControlPts(int index) {
        // initialize the variables
        Coord expectedPoint1 = new Coord();
        Coord expectedPoint2 = new Coord();
        CubicCurve seg1 = null, seg2 = null;
        Coord point;

        // special case for the Start point
        if(index == 0){
            seg2 = segments.get(0);
            point = StartPoint;
        }
        // special case for the End point
        else if(index == segments.size()){
            seg1 = segments.get(index-1);
            point = EndPoint;

        }else {
            // find the curve segment (that is, with index of (index - 1) in segments)
            seg1 = segments.get(index - 1);
            seg2 = segments.get(index);
            point = points.get(index);
        }

        double limit = 40;
        double diff = 30;

        // set the expected Point location
        if (point.getX() > limit) {
            expectedPoint1.setX(point.getX() - diff);
            expectedPoint1.setY(point.getY());
            expectedPoint2.setX(point.getX() + diff);
            expectedPoint2.setY(point.getY());
        } else {
            if (point.getY() > limit) {
                expectedPoint1.setX(point.getX());
                expectedPoint1.setY(point.getY() - diff);
                expectedPoint2.setX(point.getX());
                expectedPoint2.setY(point.getY() + diff);
            } else {
                // both x and y are too little, we add diff
                // get a diagonal line
                expectedPoint1.setX(point.getX());
                expectedPoint1.setY(point.getY() + diff);
                expectedPoint2.setX(point.getX() + diff);
                expectedPoint2.setY(point.getY());
            }
        }

        // if the previous point is on the left, then the expected point 1 is on the left
        // vice verse
        boolean order = true;
        if(index>0 && points.get(index-1).getX() > point.getX()){
            order = false;
        }

        // finally modify the point and
        // change the cubic curve as well
        if(seg1 != null){
            if(order){
                setControlPt(point, true, expectedPoint1);
                seg1.setControlX2(expectedPoint1.getX());
                seg1.setControlY2(expectedPoint1.getY());
            }else{
                setControlPt(point, true, expectedPoint2);
                seg1.setControlX2(expectedPoint2.getX());
                seg1.setControlY2(expectedPoint2.getY());
            }
            // tell the model that the drawing is changed
            model.unsaved();
        }
        if(seg2 != null){
            if(order) {
                setControlPt(point, false, expectedPoint2);
                seg2.setControlX1(expectedPoint2.getX());
                seg2.setControlY1(expectedPoint2.getY());
            }else{
                setControlPt(point, false, expectedPoint1);
                seg2.setControlX1(expectedPoint1.getX());
                seg2.setControlY1(expectedPoint1.getY());
            }
            // tell the model that the drawing is changed
            model.unsaved();
        }
    }

    public void deleteObject(){
        for(Coord point : points){
            point.deleteThis();
        }
        for(CubicCurve curve : segments){
            curve.setVisible(false);
            root.getChildren().remove(curve);
        }
        bound.setVisible(false);
        root.getChildren().remove(bound);
        root = null;
        model.getCurves().remove(this);
        // tell the model that the drawing is changed
        model.unsaved();
        model = null;
    }


    // -------- for saving and loading -------------
    public CurveStats getCurveStat(){
        CurveStats curveStats = new CurveStats();

        // insert all important data
        // store all the points on the curve
        for(Coord point : points){
            CoordStats cs = point.getCoordStat();
            curveStats.points.add(cs);
        }

        // store all the segments of cubic curves
        for(CubicCurve cc : segments){
            CubicCurveStats ccs = new CubicCurveStats(cc);
            curveStats.segments.add(ccs);
        }

        // this can be retrieved from point list
        // public CoordStats StartPoint;
        // public CoordStats EndPoint;

        // not used yet
        // private Rectangle bound;

        // Properties of the curve
        curveStats.color = new ColorStats(this.color);
        curveStats.thickness = this.thickness;
        curveStats.pattern = this.pattern;

        return curveStats;
    }

    Curve(Model model, Group root, CurveStats curveStats){
        // initialize
        // this usually means there are only two points
        bound = new Rectangle(0,0,0,0);
        bound.setVisible(false);

        this.model = model;
        this.setRoot(root);

        // insert all important data
// store all the points on the curve
        // need to get Start and End point
        int counter = 0;
        for(CoordStats point : curveStats.points){
            Coord c = point.getObject(root);
            this.points.add(c);
            if(counter == 0){
                this.StartPoint = c;
            }
            if(counter == curveStats.points.size()-1){
                this.EndPoint = c;
            }
            // set control points
            if(point.control1 != null){
                // repeated adding
                setControlPt(c,true,point.control1.getCoordinate());
            }
            if(point.control2 != null){
                setControlPt(c,false,point.control2.getCoordinate());
            }
            // set listener for the point
            addListenerToPt(c);
            counter++;
        }

        // store all the segments of cubic curves
        for(CubicCurveStats ccs : curveStats.segments){
            CubicCurve cc = ccs.getObject(root);
            cc.setFill(fill);
            this.segments.add(cc);

            // set listeners
            addListenerToSeg(cc);
        }

        // Properties of the curve
        this.color = curveStats.color.getObject();
        this.thickness = curveStats.thickness;
        this.pattern = curveStats.pattern;

        this.selected = false;
        this.turnOffVisualIndication();
        updateAll();
    }
}
