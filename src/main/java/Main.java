import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.*;
import javafx.stage.Stage;

// Main
public class Main extends Application {
    @Override
    public void start(Stage stage) throws Exception {

        stage.setTitle("Drawing Panel For Bezier Curves");

        // create and initialize the Model to hold our counter
        Model model = new Model(stage);

        LayoutView layout = new LayoutView(model);
        BorderPane mainPane = layout.getPane();

        // Add grid to a scene (and the scene to the stage)
        Scene scene = new Scene(mainPane, model.getWindowWidth(), model.getWindowHeight());
        stage.setScene(scene);
        stage.show();
        model.setScene(scene);
        model.updateWindow();
    }
}
