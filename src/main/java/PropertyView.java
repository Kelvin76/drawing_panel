import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;

import java.util.ArrayList;

public class PropertyView extends GridPane implements IView{

    // reference to the model
    private Model model;

    // some config
    private Background c_bg = new Background(new BackgroundFill(Color.AZURE, CornerRadii.EMPTY, Insets.EMPTY));
    private Border c_bd = new Border(new BorderStroke(Color.DARKGREY, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(1)));

    private static double padding = 5;
    private static double gap = 4;
    private static double maxGap = 8;
    private static double minGap = 1;

    private final double buttonMaxHeight = 100;
    private final double buttonMaxWidth = 120;
    private final double buttonMinHeight = 30;
    private final double buttonMinWidth = 40;
    private double buttonHeight;
    private double buttonWidth;
    private ArrayList<ToggleButton> btnList = new ArrayList<>();
    private final ToggleGroup alignThickGroup = new ToggleGroup();
    private final ToggleGroup alignStylekGroup = new ToggleGroup();
    /* -------- palette components -------- */
    private ColorPicker colorPicker = new ColorPicker();

    private ToggleButton lineThickness1 = new ToggleButton("thickness 1");
    private ToggleButton lineThickness2 = new ToggleButton("thickness 2");
    private ToggleButton lineThickness3 = new ToggleButton("thickness 3");
    private ToggleButton lineThickness4 = new ToggleButton("thickness 4");

    private double thickness1 = 1;
    private double thickness2 = 3;
    private double thickness3 = 6;
    private double thickness4 = 9;

    private ToggleButton lineStyle1 = new ToggleButton("Style 1");
    private ToggleButton lineStyle2 = new ToggleButton("Style 2");
    private ToggleButton lineStyle3 = new ToggleButton("Style 3");
    private ToggleButton lineStyle4 = new ToggleButton("Style 4");

    Line thick1 = new Line(0,0,35,20);
    Line thick2 = new Line(0,0,33,19);
    Line thick3 = new Line(0,0,31,17);
    Line thick4 = new Line(0,0,26,14);

    Line style1 = new Line(0,0,70,45);
    Line style2 = new Line(0,0,70,45);
    Line style3 = new Line(0,0,70,45);
    Line style4 = new Line(0,0,70,45);

    PropertyView(Model model) {
        // configure the settings of this pane
        this.setBackground(c_bg);
        this.setBorder(c_bd);

        // keep track of the model
        this.model = model;

        // initialize the layout
        this.setPadding(new Insets(padding));
        this.setHgap(gap);
        this.setVgap(gap);

        colorPicker.getStyleClass().add("button");
        colorPicker.setLayoutX(padding);
        colorPicker.setLayoutY(padding);

        btnList.add(lineThickness1);
        btnList.add(lineThickness2);
        btnList.add(lineThickness3);
        btnList.add(lineThickness4);
        lineThickness1.setToggleGroup(alignThickGroup);
        lineThickness2.setToggleGroup(alignThickGroup);
        lineThickness3.setToggleGroup(alignThickGroup);
        lineThickness4.setToggleGroup(alignThickGroup);

        btnList.add(lineStyle1);
        btnList.add(lineStyle2);
        btnList.add(lineStyle3);
        btnList.add(lineStyle4);
        lineStyle1.setToggleGroup(alignStylekGroup);
        lineStyle2.setToggleGroup(alignStylekGroup);
        lineStyle3.setToggleGroup(alignStylekGroup);
        lineStyle4.setToggleGroup(alignStylekGroup);
        // modify each button
        for(ToggleButton btn : btnList){
            btn.setLayoutX(padding);
            btn.setLayoutY(padding);
            btn.setMinWidth(buttonMinWidth);
            btn.setMaxWidth(buttonMaxWidth);
            btn.setMinHeight(buttonMinHeight);
            btn.setMaxHeight(buttonMaxHeight);
            // wrap text
            btn.setWrapText(true);
            // set some common events
            // add shadow when enter
            btn.setOnMouseEntered(MouseEvent-> {
                btn.setEffect(new DropShadow());
            });
            // remove shadow when mouse exited
            btn.setOnMouseExited(MouseEvent -> {
                btn.setEffect(null);
            });

        }

        // add graphics for buttons
        thick1.setStrokeWidth(thickness1);
        thick2.setStrokeWidth(thickness2);
        thick3.setStrokeWidth(thickness3);
        thick4.setStrokeWidth(thickness4);
        lineThickness1.setGraphic(thick1);
        lineThickness2.setGraphic(thick2);
        lineThickness3.setGraphic(thick3);
        lineThickness4.setGraphic(thick4);
        lineThickness1.setContentDisplay(ContentDisplay.TOP);
        lineThickness2.setContentDisplay(ContentDisplay.TOP);
        lineThickness3.setContentDisplay(ContentDisplay.TOP);
        lineThickness4.setContentDisplay(ContentDisplay.TOP);

        style2.getStrokeDashArray().addAll(15d,25d);
        style3.getStrokeDashArray().addAll(10d);
        style4.getStrokeDashArray().addAll(30d,15d,10d,15d);
        style1.getStrokeDashArray().addAll(1d);
        lineStyle1.setGraphic(style1);
        lineStyle2.setGraphic(style2);
        lineStyle3.setGraphic(style3);
        lineStyle4.setGraphic(style4);
        lineStyle1.setContentDisplay(ContentDisplay.TOP);
        lineStyle2.setContentDisplay(ContentDisplay.TOP);
        lineStyle3.setContentDisplay(ContentDisplay.TOP);
        lineStyle4.setContentDisplay(ContentDisplay.TOP);

        this.add(colorPicker, 0, 0);

        this.add(lineThickness1, 0, 1);
        this.add(lineThickness2, 1, 1);
        this.add(lineThickness3, 2, 1);
        this.add(lineThickness4, 3, 1);

        this.add(lineStyle1, 0, 2);
        this.add(lineStyle2, 1, 2);
        this.add(lineStyle3, 2, 2);
        this.add(lineStyle4, 3, 2);

        // register with the model when we're ready to start receiving data
        model.addView(this);

        // set up events
        // update the recorded color
        colorPicker.valueProperty().addListener(new ChangeListener<Color>() {
            @Override
            public void changed(ObservableValue<? extends Color> observable, Color oldValue, Color newValue) {
                model.setCoPickerColor(newValue);
            }
        });

        // update the thickness
        lineThickness1.setOnAction(ActionEvent->{
            model.setThickness(thickness1);
        });
        lineThickness2.setOnAction(ActionEvent->{
            model.setThickness(thickness2);
        });
        lineThickness3.setOnAction(ActionEvent->{
            model.setThickness(thickness3);
        });
        lineThickness4.setOnAction(ActionEvent->{
            model.setThickness(thickness4);
        });

        // update the line style
        lineStyle1.setOnAction(ActionEvent->{
            model.setPattern(0);
        });
        lineStyle2.setOnAction(ActionEvent->{
            model.setPattern(1);
        });
        lineStyle3.setOnAction(ActionEvent->{
            model.setPattern(2);
        });
        lineStyle4.setOnAction(ActionEvent->{
            model.setPattern(3);
        });

    }

    // set the size of this view
    public void setLayout(){
        double width = Math.max(buttonWidth,buttonMinWidth);
        double height = Math.max(buttonHeight,buttonMinHeight);

        colorPicker.setPrefWidth(width);
        colorPicker.setPrefHeight(height);

        // modify the size
        for(ToggleButton btn : btnList){
            btn.setPrefWidth(buttonWidth);
            btn.setPrefHeight(buttonHeight);
        }
    }

    @Override
    public void updateView() {
        // set the size of the components inside
        gap = (model.getLeftBarWidth() < this.getHeight())? model.getLeftBarWidth()/70 : this.getHeight()/70;
        gap = Math.max(gap,minGap);
        gap = Math.min(gap,maxGap);
        this.setHgap(gap);
        this.setVgap(gap);
        this.buttonWidth = model.getLeftBarWidth() / 2 - padding - gap;
        this.buttonHeight = this.getHeight() / 3 - padding - gap;
        buttonWidth = Math.min(buttonWidth,buttonMaxWidth);
        buttonHeight = Math.min(buttonHeight,buttonMaxHeight);
        this.setLayout();

        // update the components

        // update the color for the color picker
        colorPicker.setValue(model.getCoPickerColor());
        thick1.setStroke(colorPicker.getValue());
        thick2.setStroke(colorPicker.getValue());
        thick3.setStroke(colorPicker.getValue());
        thick4.setStroke(colorPicker.getValue());
        style1.setStroke(colorPicker.getValue());
        style2.setStroke(colorPicker.getValue());
        style3.setStroke(colorPicker.getValue());
        style4.setStroke(colorPicker.getValue());

        // update the length of the lines
        double newX = buttonWidth - 50;
        double newY = buttonHeight - 30;

        style1.setEndX(newX);
        style1.setEndY(newY);

        style2.setEndX(newX);
        style2.setEndY(newY);

        style3.setEndX(newX);
        style3.setEndY(newY);

        style4.setEndX(newX);
        style4.setEndY(newY);

        // update the button states
        // line thickness
        double thickness = model.getThickness();
        if(thickness == thickness1){
            lineThickness1.setSelected(true);
        }else if(thickness == thickness2){
            lineThickness2.setSelected(true);
        }else if(thickness == thickness3){
            lineThickness3.setSelected(true);
        }else if(thickness == thickness4){
            lineThickness4.setSelected(true);
        }

        // line style
        int lineStyle = model.getPattern();
        if(lineStyle == 0){
            lineStyle1.setSelected(true);
        }else if(lineStyle == 1){
            lineStyle2.setSelected(true);
        }else if(lineStyle == 2){
            lineStyle3.setSelected(true);
        }else if(lineStyle == 3){
            lineStyle4.setSelected(true);
        }
    }
}
