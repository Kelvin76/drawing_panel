import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;

public class Coord {
    // some constants
    private static Color circleSmoothColor = Color.CORNFLOWERBLUE;
    private static Color circleSharpColor = Color.DARKGOLDENROD;
    private static Color circleFill = Color.WHITE;
    private static double circleSThickness = 2;
    private static double circleRadius = 6;
    private static double radiusCoefficient = 1;

    private double x = 0 ;
    private double y = 0;

    private Circle circle = new Circle();

    private Coord control1 = null;
    private Coord control2 = null;
    transient private Line lineToC1 = null;
    transient private Line lineToC2 = null;

    // Canvas reference
    transient public Group root = null;

    // Point property
    boolean isSmooth = true;
    boolean isShown = true;

    Coord(){ /* only used temporary */ }

    Coord(double x, double y){
        /* only used temporary */
        this.x = x;
        this.y = y;
    }

    Coord(double x, double y, Group root){
        this.setView(x,y);
        this.initCircle();
        this.setRoot(root);

        setEventListeners();
    }

    Coord(Coord c, Group root){
        this(c.getX(),c.getY(),root);
    }

    // listeners
    private void setEventListeners(){
        // some basic events
        // set cursor type for circle
        // need to be able to move the circle
    }

    public void setView(double x, double y){
        this.x = x;
        this.y = y;
        circle.setCenterX(x);
        circle.setCenterY(y);
    }

    // mutators
    public void initCircle(){
        // configure the circle type
        circle.setStroke(circleSmoothColor);
        circle.setFill(circleFill);
        circle.setStrokeWidth(circleSThickness);
        circle.setRadius(radiusCoefficient*circleRadius);
    }

    public void setX(double x){
        // System.out.println("Set x from "+this.x+" to "+x);
        this.x = x;
        circle.setCenterX(x);
    }
    public void setY(double y){
        // System.out.println("Set y from "+this.y+" to "+y);
        this.y = y;
        circle.setCenterY(y);
    }
    // returns true if the point alreay exists
    public boolean setControl1(Coord c){
        //System.out.println("Set control 1");
        // delete the control point first if not null
        if(control1 != null){
            //control1.turnOffDisplay();
            control1.setX(c.getX());
            control1.setY(c.getY());
            lineToC1.setStartX(c.getX());
            lineToC1.setStartY(c.getY());
            lineToC1.setEndX(this.x);
            lineToC1.setEndY(this.y);
            return true;
        }
        //System.out.println("Need to initialize control 1");
        control1 = new Coord(c,root);
        lineToC1 = new Line(control1.getX(),control1.getY(),this.x,this.y);
        root.getChildren().add(lineToC1);
        // need to set listener for control point
        return false;
    }
    public boolean setControl2(Coord c){
        //System.out.println("Set control 2");
        // modify the point
        if(control2 != null){
            control2.setX(c.getX());
            control2.setY(c.getY());
            lineToC2.setStartX(c.getX());
            lineToC2.setStartY(c.getY());
            lineToC2.setEndX(this.x);
            lineToC2.setEndY(this.y);
            return true;
        }
        //System.out.println("Need to initialize control 2");
        control2 = new Coord(c,root);
        lineToC2 = new Line(control2.getX(),control2.getY(),this.x,this.y);
        root.getChildren().add(lineToC2);
        // need to set listener for control point
        return false;
    }

    // delete the control point and the line to it
    public void deleteControlPoint(boolean isOne){
        if(isOne){
            if(control1 != null){
                control1.deleteThis();
                control1 = null;
                lineToC1.setVisible(false);
                root.getChildren().remove(lineToC1);
                lineToC1 = null;
            }
        }else if(control2 != null){
            control2.deleteThis();
            control2 = null;
            lineToC2.setVisible(false);
            root.getChildren().remove(lineToC2);
            lineToC2 = null;
        }
    }

    public void setRoot(Group root){
        if(this.root == null && root != null){
            //System.out.println("Set root for Coord:"+this+ ", to: "+root);
            this.root = root;
            root.getChildren().add(circle);
            circle.toBack();
        }
    }

    public boolean isSmooth(){
        return isSmooth;
    }

    // returns the final state of smooth or not
    public boolean toggleType(){
        if(isSmooth){
            // change it to sharp
            setSharp();
        }else{
            // change it to smooth
            setSmooth();
        }
        return isSmooth;
    }

    private void setSharp(){
        isSmooth = false;
        // actually, it only set control points to invisible
        circle.setStroke(circleSharpColor);
        turnOffChildren();
    }
    private void setSmooth(){
        isSmooth = true;
        // actually, it only set control points to visible
        circle.setStroke(circleSmoothColor);
        turnOnChildren();
    }

    // accessors
    public double getX(){return x;}
    public double getY(){return y;}
    public Coord getControl1(){return control1;}
    public Coord getControl2(){return control2;}
    public Line getLine1(){return lineToC1;}
    public Line getLine2(){return lineToC2;}
    public Circle getCircle(){return circle;}
    // Class public functions

    // show visual indication
    public void turnOnDisplay(){
        // turn on current node circle
        if(circle != null) {
            circle.setVisible(true);
        }
        turnOnChildren();
        isShown = true;
    }

    private void turnOnChildren(){
        // if has control points (this is a node)
        if(!isSmooth){
            // if sharp, then show no children
            turnOffChildren();
        }else{
            if(control1 != null){
                control1.turnOnDisplay();
                lineToC1.setVisible(true);
            }
            if(control2 != null){
                control2.turnOnDisplay();
                lineToC2.setVisible(true);
            }
        }
    }

    public void turnOffDisplay(){
        if(circle != null){
            this.circle.setVisible(false);
        }
        turnOffChildren();
        isShown = false;
    }

    private void turnOffChildren(){
        if(control1 != null){
            control1.turnOffDisplay();
            lineToC1.setVisible(false);
        }
        if(control2 != null){
            control2.turnOffDisplay();
            lineToC2.setVisible(false);
        }
    }

    // pass in a coefficient for the radius
    public void setCircleRadius(double coe){
        radiusCoefficient = Math.max(Math.cbrt(coe),1);
        this.circle.setRadius(radiusCoefficient*circleRadius);
        if(control1!=null){
            control1.setCircleRadius(coe);
        }
        if(control2!=null){
            control2.setCircleRadius(coe);
        }
    }


    // TODO:mark
    public void deleteThis(){
        this.turnOffDisplay();
        deleteControlPoint(true);
        deleteControlPoint(false);
        if(circle != null){
            circle.setVisible(false);
            circle.setId("DELETE");
        }
    }

    // helpers
    @Override
    public String toString() {
        return "("+this.x+","+this.y+")";
    }

    // -------- for saving and loading -------------
    public CoordStats getCoordStat(){
        CoordStats newCoordStat = new CoordStats();

        // store all important info
        newCoordStat.x = this.x;
        newCoordStat.y = this.y;

        if(control1 != null){
            newCoordStat.control1 = this.control1.getCoordStat();
        }
        if(control2 != null){
            newCoordStat.control2 = this.control2.getCoordStat();
        }

        // Point property
        newCoordStat.isSmooth = this.isSmooth;
        newCoordStat.isShown = this.isShown;

        return newCoordStat;
    }

    Coord(Group root, CoordStats coordStats){
        // initialize this point
        this.setView(coordStats.x,coordStats.y);
        this.initCircle();
        this.setRoot(root);
        setEventListeners();

        // setting up other fields
        this.isSmooth = coordStats.isSmooth;
        if(isSmooth){
            this.setSmooth();
        }else{
            this.setSharp();
        }
        this.isShown = coordStats.isShown;
        if(isShown) {
            this.turnOnDisplay();
        }else{
            this.turnOffDisplay();
        }
    }

}
