import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.geometry.Rectangle2D;
import javafx.scene.*;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.WritableImage;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.paint.Color;
import javafx.scene.shape.CubicCurve;
import javafx.stage.FileChooser;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javax.imageio.ImageIO;

import java.awt.image.RenderedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

// TODO:
// might add F5 for quick save

public class Model {

    enum BTN{
        PEN,
        SELECT,
        POINTTYPE,
        ERASE,
        ADDPT,
        REMOVPT
    }

    enum STATE{
        NA,
        DRAWING,
        SELECTING,
        POINTTYPECHANGING,
        ERASING,
        ADDING,
        REMOVING
    }

    // some unique object instances
    private final Stage stage;
    private Scene scene;
    private CanvasView canvas;
    private final FileChooser fileChooserSaver = new FileChooser();
    private final FileChooser fileChooserLoader = new FileChooser();

    // some constants
    private final static double leftBarPercent = 0.3;
    private final static double leftBarMinWidth = 125;
    private final static double leftBarMaxWidth = 400;
    private final static double toolBarPercent = 0.3;
    private final static double toolBarMinHeight = 120;
    private final static double toolBarMaxHeight = 400;

    // Size properties of some layouts
    private static double leftBarWidth;
    private static double toolBarHeight;

    private static double WINDOW_WIDTH;
    private static double WINDOW_HEIGHT;

    private static final double WINDOW_WIDTH_OFFSET = 150;
    private static final double WINDOW_HEIGHT_OFFSET = 250;

    // Data
    // store all the segments of cubic curves
    List<Curve> allCurves = new ArrayList<>();
    private static Curve currentSelectedCurve = null;

    private static Color coPickerColor = Color.BLACK;
    private static double choseThickness = 1;
    private static int chosePattern = 0;

    private static Coord lastPoint = null;

    private static boolean unsaved = false;

    // this indicates how many times an invalid button is pressed
    private static int wrongBtnPressedCount = 0;

    // states
    private static STATE mode = STATE.NA;
    private static boolean mousePressed = false;

    // line properties
    private static Color currentLineColor = Color.BLACK;
    private static double currentLineThickness = 1;
    private static int currentLinePattern = 0;

    // all views of this model
    private ArrayList<IView> views = new ArrayList<IView>();

    Model(Stage stage) {
        this.stage = stage;

        // initialize the window size according to the screen size
        Rectangle2D screenRectangle = Screen.getPrimary().getBounds();
        this.WINDOW_WIDTH = Math.min(1200,screenRectangle.getWidth() - WINDOW_WIDTH_OFFSET);
        this.WINDOW_HEIGHT = Math.min(800,screenRectangle.getHeight() - WINDOW_HEIGHT_OFFSET);
        updateWindow();

        // set the file chooser saver
        fileChooserSaver.setInitialDirectory(new File("/"));
        fileChooserSaver.setTitle("Save file");
        fileChooserSaver.setInitialFileName("My_Curve");
        fileChooserSaver.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("What is this file type?","*.watsdis"),
                new FileChooser.ExtensionFilter("Just another choice","*.tzt"),
                new FileChooser.ExtensionFilter("png files (*.png)", "*.png"));

        // set the file chooser loader
        fileChooserLoader.setInitialDirectory(new File("/"));
        fileChooserLoader.setTitle("Load file");
        fileChooserLoader.setInitialFileName("My_Curve");
        fileChooserLoader.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("text file","*.watsdis","*.tzt","*.txt", "*.doc"),
                new FileChooser.ExtensionFilter("Image file","*.png"));

        // listener to window close event
        stage.setOnCloseRequest(ActionEvent -> {
            // if the canvas is unsaved, prompt
            if(unsaved){
                // show confirm box
                boolean answer = ConfirmBox.Display("Confirm", "You have unsaved drawing. Do you still want to exit the application?");
                if (!answer) {
                    ActionEvent.consume();    // window close event stops here
                    return;
                }
            }
        });

    }

    /* --------------  Helper functions -------------- */
    private void resetStates(){
        mode = STATE.NA;
        if(currentSelectedCurve != null){
            if(!currentSelectedCurve.validCurve()){
                // if the curve is not initiated, delete it
                allCurves.remove(currentSelectedCurve);
            }
            currentSelectedCurve.unselectIt();
        }
        currentSelectedCurve = null;
        lastPoint = null;
        // change the property panel back
        currentLineColor = coPickerColor;
        currentLineThickness = choseThickness;
        currentLinePattern = chosePattern;

        wrongBtnPressedCount = 0;
    }

    private void updateValues(){
        leftBarWidth = leftBarPercent*(WINDOW_WIDTH+WINDOW_WIDTH_OFFSET);
        toolBarHeight = toolBarPercent*(WINDOW_HEIGHT+WINDOW_HEIGHT_OFFSET);
    }

    private void addListener(){
        // set key combination for Exiting the drawing mode
        KeyCombination kcExit = new KeyCodeCombination(KeyCode.ESCAPE, KeyCombination.CONTROL_ANY);
        this.scene.getAccelerators().put(kcExit, new Runnable() {
            @Override
            public void run() {
                // if drawing mode, we exit
                switch (mode){
                    case DRAWING:
                    case SELECTING:
                    case POINTTYPECHANGING:
                    case REMOVING:
                    case ADDING:
                    case ERASING:
                        // reset the states
                        resetStates();
                        notifyALL();
                        break;
                    default:

                }
            }
        });

        // set key combination for Deleting the shape
        KeyCombination kcDelete = new KeyCodeCombination(KeyCode.DELETE, KeyCombination.CONTROL_ANY);
        this.scene.getAccelerators().put(kcDelete, new Runnable() {
            @Override
            public void run() {
                if(currentSelectedCurve != null){
                    for(Curve curve :allCurves){
                        if(curve == currentSelectedCurve){
                            curve.deleteObject();
                            currentSelectedCurve = null;
                            break;
                        }
                    }
                }
            }
        });

        // get mouse clicked and un clicked
        scene.setOnMousePressed(MouseEvent->{
            mousePressed = true;
        });
        scene.setOnMouseReleased(MouseEvent->{
            mousePressed = false;
        });

        // window width updated
        scene.widthProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                WINDOW_WIDTH = newValue.doubleValue() - WINDOW_WIDTH_OFFSET;
                updateWindow();
            }
        });

        // window height updated
        scene.heightProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                WINDOW_HEIGHT = newValue.doubleValue() - WINDOW_HEIGHT_OFFSET;
                updateWindow();
            }
        });
    }


    /* --------------  Mutators -------------- */
    public void setCoPickerColor(Color color){
        if(currentSelectedCurve != null){
            currentSelectedCurve.setColor(color);
            unsaved();
        }else{
            coPickerColor = color;
        }
        currentLineColor = color;
        notifyALL();
    }
    public void setThickness(double thickness){
        if(currentSelectedCurve != null){
            currentSelectedCurve.setThickness(thickness);
            unsaved();
        }else {
            choseThickness = thickness;
        }
        currentLineThickness = thickness;
        notifyALL();
    }
    public void setPattern(int pattern){
        if(currentSelectedCurve != null){
            currentSelectedCurve.setPattern(pattern);
            unsaved();
        }else {
            chosePattern= pattern;
        }
        currentLinePattern = pattern;
        notifyALL();
    }

    public void setLineColor(Color color){
        currentLineColor = color;
    }
    public void setLineThickness(double thickness){
        currentLineThickness = thickness;
    }
    public void setLinePattern(int pattern){
        currentLinePattern = pattern;
    }

    public void setScene(Scene scene){
        this.scene = scene;
        // add the listeners
        addListener();

        WINDOW_WIDTH = Math.min(1600,scene.widthProperty().getValue() - WINDOW_WIDTH_OFFSET);
        WINDOW_HEIGHT = Math.min(1200,scene.heightProperty().getValue() - WINDOW_HEIGHT_OFFSET);
        updateWindow();
    }

    /* --------------  Accessors -------------- */

    // get the current selected curve
    public Curve getCurrentSelectedCurve(){
        return currentSelectedCurve;
    }

    // get all the curves
    public List<Curve> getCurves(){
        return this.allCurves;
    }

    public Color getCoPickerColor(){
        if(currentSelectedCurve != null){
            return currentLineColor;
        }
        return coPickerColor;
    }
    public double getThickness(){
        if(currentSelectedCurve != null){
            return currentLineThickness;
        }
        return choseThickness;
    }
    public int getPattern(){
        if(currentSelectedCurve != null){
            return currentLinePattern;
        }
        return chosePattern;
    }

    // get the width of the window
    public double getWindowWidth(){
        return WINDOW_WIDTH;
    }
    // get the height of the window
    public double getWindowHeight(){
        return WINDOW_HEIGHT;
    }
    // get the width of the left bar
    public double getLeftBarWidth(){
        return Math.min(leftBarMaxWidth,Math.max(leftBarWidth,leftBarMinWidth));
    }
    // get the height of the left bar
    public double getLeftBarHeight(){
        return Math.min(toolBarMaxHeight,Math.max(toolBarHeight,toolBarMinHeight));
    }

    public Model.STATE getMode(){
        return mode;
    }
    public int getWrongButtonCount(){return wrongBtnPressedCount;}
    public void setWrongBtnPressedCount(int num){wrongBtnPressedCount = num;}

    /* --------------  Public functions -------------- */
    public void updateWindow(){
        this.updateValues();
        this.notifyALL();
    }

    // a line is selected, so we unselect the previous curve
    public void lineSelected(Curve c){

        // need to be in line select mode to select a line
        switch(mode){
            case SELECTING:
                if(currentSelectedCurve == c){
                    // selected the same line
                    return;
                }
                //System.out.println("Should have selected this now.");
                if(currentSelectedCurve != null){
                    currentSelectedCurve.unselectIt();
                }
                currentSelectedCurve = c;
                // change the property board
                currentLineColor = currentSelectedCurve.getColor();
                currentLineThickness = currentSelectedCurve.getThickness();
                currentLinePattern = currentSelectedCurve.getPattern();
                // select it
                currentSelectedCurve.selectIt();
                notifyALL();
                break;
            case ERASING:
                // if a line is selected in erasing mode, delete it
                deleteCurve(c);
            default:
        }
    }

    public void deleteCurve(Curve c){
        for(Curve curve :allCurves){
            if(curve == c){
                curve.deleteObject();
                break;
            }
        }
    }

    // a line is hovered, so we might want to delete it
    public void lineHovered(CubicCurve cc, Curve c){
        // if mode if Selecting, and the curve is not selected, change cursor
        if(mode == STATE.SELECTING && currentSelectedCurve != c){
            cc.setCursor(Cursor.HAND);
            c.onHover();
        }
    }

    public void canvasNewMouseClick(Coord c){
        //System.out.println("Mouse Clicked on Canvas: (x,y):" + c+". Mode:"+mode.name());
        Coord newCoord = c;
        switch(mode){
            case DRAWING:
                // needs to create a point on the canvas
                // if already drawing a curve
                if(currentSelectedCurve != null){

                    // if we are drawing this graph currently
                    if(lastPoint != null){
                        // add segment
                        currentSelectedCurve.addSegment(newCoord);
                        lastPoint = newCoord;
                        this.notifyALL();
                    }
                    // the curve is selected, but the last active point is null
                    // meaning we are have done drawing it, do nothing

                }else{
                    // System.out.println("Create new curve.");

                    // create a new curve
                    currentSelectedCurve = new Curve(this, canvas.getCanvas(),newCoord,coPickerColor,choseThickness,chosePattern);
                    // add the curve into the list
                    allCurves.add(currentSelectedCurve);
                    lastPoint = newCoord;
                    this.notifyALL();
                }

                break;
            case ERASING:
                break;
            default:
        }

    }

    // set the style of the cursor, boolean set means to set it or clear it
    public void setCursorType(boolean set){
        if(!set){
            scene.setCursor(Cursor.DEFAULT);
        }else{
            switch (mode){
                case DRAWING:
                    scene.setCursor(Cursor.CROSSHAIR);
                    break;
                case ERASING:
                    //scene.setCursor(eraseCursor);
                    scene.setCursor(Cursor.DEFAULT);
                    break;
                case SELECTING:
                    scene.setCursor(Cursor.DEFAULT);
                    break;
                case POINTTYPECHANGING:
                    scene.setCursor(Cursor.DEFAULT);
                    break;
                case NA:
                default:
                    scene.setCursor(Cursor.DEFAULT);
            }
        }
    }

    // mark the program as unsaved
    public void unsaved(){
        stage.setTitle("Drawing Panel For Bezier Curves*");
        unsaved = true;
    }

    // mark the program as saved
    public void saved(){
        stage.setTitle("Drawing Panel For Bezier Curves");
        unsaved = false;
    }

    public void newFile(ActionEvent event){
        if(unsaved){
            // show confirm box
            boolean answer = ConfirmBox.Display("Confirm", "You have unsaved drawing. Do you still want to clear the canvas?");
            if (!answer) {
                event.consume();    // window close event stops here
                return;
            }
        }
        // empty the canvas
        // clear the current canvas

        // basically reset everything
        clearCanvas();
        saved();
        resetStates();
        notifyALL();
    }

    public void saveFile(){

        // Serialization
        try{
            // choose the file
            File file = fileChooserSaver.showSaveDialog(stage);
            // exit if file is not set
            if(file == null){
                return;
            }

            String fileName = file.getName();
            String fileExtension = fileName.substring(fileName.lastIndexOf(".") + 1, file.getName().length());

            if(fileExtension.equals("png")){
                WritableImage writableImage = new WritableImage((int)canvas.getWidth(),
                        (int)canvas.getHeight());
                canvas.snapshot(null, writableImage);
                RenderedImage renderedImage = SwingFXUtils.fromFXImage(writableImage, null);
                //Write the snapshot to the chosen file
                ImageIO.write(renderedImage, "png", file);
            }else{
                // save as tmp file
                // open output stream
                ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file));
                // serialize
                // save the needed information
                allCurveStats data = new allCurveStats(allCurves);
                // write the data into the output file
                oos.writeObject(data);
                oos.close();
            }

            // finished
            saved();
            notifyALL();
        }catch (Exception ex){
            System.out.println("[Exeception] exception happened while saving the file. "+ex.toString());
        }
    }

    public void loadFile(ActionEvent event){
        if(unsaved){
            // show confirm box
            boolean answer = ConfirmBox.Display("Confirm", "You have unsaved drawing. Do you still want to load a file?");
            if (!answer) {
                event.consume();    // window close event stops here
                return;
            }
        }

        // Deserialization
        try{
            // choose the file
            File file = fileChooserLoader.showOpenDialog(stage);
            // exit if file is not chose
            if(file == null){
                return;
            }

            // receive the object from file
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
            Object data = ois.readObject();

            // clear the current canvas
            clearCanvas();

            // deserialize
            allCurves = ((allCurveStats)(data)).getObject(this,canvas.getCanvas());
            // load the canvas
            canvas.loadCanvas(allCurves);
            ois.close();

            // finished
            saved();
            notifyALL();
        }
        catch (StreamCorruptedException | InvalidClassException ex){
            // the file format is not supported
            System.out.println("[Exeception] Target file is not readable for this application. "+ex.toString());
        } catch (Exception ex){
            System.out.println("[Exeception] exception happened while loading the file. "+ex.toString());
        }
    }

    public void quitProgram(ActionEvent event){

        if(unsaved){
            // show confirm box
            boolean answer = ConfirmBox.Display("Confirm", "You have unsaved drawing. Do you still want to quit the application?");
            if (!answer) {
                event.consume();    // window close event stops here
                return;
            }
        }

        Platform.exit();
    }

    private void clearCanvas(){
        // clear the current canvas
        for(Curve c : allCurves){
            c.turnOffVisualIndication();
            //c.deleteObject();
        }
        allCurves.clear();
        canvas.removeCanvas();
    }

    /* ------------- MVC related functions ------------- */
    // deal with the button pressed events
    public void buttonPressed(BTN btnState, ToggleButton btn){
        // System.out.println("Button pressed, "+ btnState.name());
        switch(btnState){
            case PEN:
                // enter drawing mode
                if(mode == STATE.DRAWING){
                    resetStates();
                }else{
                    resetStates();
                    mode = STATE.DRAWING;
                }
                break;
            case SELECT:
                // enter selecting mode
                if(mode == STATE.SELECTING){
                    resetStates();
                }else{
                    // resetStates();
                    mode = STATE.SELECTING;
                }
                break;
            case ERASE:
                // enter erasing mode
                if(mode == STATE.ERASING){
                    resetStates();
                }else{
                    resetStates();
                    mode = STATE.ERASING;
                }
                break;
            case POINTTYPE:
                // need to select a curve first
                if(currentSelectedCurve != null){
                    if(mode == STATE.POINTTYPECHANGING){
                        resetStates();
                    }else{
                        mode = STATE.POINTTYPECHANGING;
                    }
                }else{
                    btn.setSelected(false);
                    mode = STATE.NA;
                    wrongBtnPressedCount++;
                }
                break;
            case ADDPT:
                // enter Adding mode
                // need to select a curve first
                if(currentSelectedCurve != null) {
                    if (mode == STATE.ADDING) {
                        resetStates();
                    } else {
                        mode = STATE.ADDING;
                    }
                }else{
                    btn.setSelected(false);
                    mode = STATE.NA;
                    wrongBtnPressedCount++;
                }
                break;
            case REMOVPT:
                // enter Removing mode
                // need to select a curve first
                if(currentSelectedCurve != null) {
                    if (mode == STATE.REMOVING) {
                        resetStates();
                    } else {
                        mode = STATE.REMOVING;
                    }
                }else {
                    btn.setSelected(false);
                    mode = STATE.NA;
                    wrongBtnPressedCount++;
                }
                break;
            default:
        }
        notifyALL();
        // System.out.println("Mode now: "+ mode.name());
    }

    // method that the views can use to register themselves with the Model
    // once added, they are told to update and get state from the Model
    public void addView(IView view) {
        views.add(view);
        view.updateView();
        if(view.getClass().getName() == "CanvasView"){
            canvas = (CanvasView) view;
        }
    }

    // This is a direct copy from the MVC model
    // the model uses this method to notify all of the Views that the data has changed
    // the expectation is that the Views will refresh themselves to display new data when appropriate
    private void notifyALL() {
        for (IView view : this.views) {
            view.updateView();
        }
    }
}
