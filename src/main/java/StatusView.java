import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class StatusView extends BorderPane implements IView{

    Label curMode = new Label("Current mode: NA");
    Label extraInfo = new Label();

    private Model model;

    StatusView(Model model) {
        this.model = model;
        curMode.setAlignment(Pos.CENTER);
        this.setLeft(curMode);
        extraInfo.setAlignment(Pos.CENTER);
        this.setCenter(extraInfo);

        model.addView(this);
    }


    @Override
    public void updateView() {
        switch (model.getMode()){
            case DRAWING:
                curMode.setText("Current mode: Drawing");
                break;
            case SELECTING:
                curMode.setText("Current mode: Selecting");
                break;
            case ERASING:
                curMode.setText("Current mode: Erasing");
                break;
            case ADDING:
                curMode.setText("Current mode: Adding point");
                break;
            case REMOVING:
                curMode.setText("Current mode: Removing point");
                break;
            case POINTTYPECHANGING:
                curMode.setText("Current mode: Point Type Switch");
                break;
            case NA:
            default:
                curMode.setText("Current mode: NA");
        }
        extraInfo.setTextFill(Color.BLACK);
        extraInfo.setFont(Font.font("", FontWeight.NORMAL,-1));
        extraInfo.setText("");
        int count = model.getWrongButtonCount();
        if(count == 0){
            extraInfo.setText("");
        }else if(count <4){
            extraInfo.setText("Please select a curve first to enter this mode.");
        }else if(count < 6) {
            extraInfo.setTextFill(Color.rgb(100, 200, 200));
            extraInfo.setFont(Font.font("", FontWeight.BOLD, -1));
            extraInfo.setText("You need to select a curve first! Please.......");
        }else if(count < 60){
            // A blink of three color
            switch (count % 3){
                case 1:
                    // greed
                    extraInfo.setTextFill(Color.rgb(70, 255, 70));
                    break;
                case 2:
                    // blue
                    extraInfo.setTextFill(Color.rgb(70, 70, 255));
                    break;
                default:
                    // red
                    extraInfo.setTextFill(Color.rgb(255, 70, 70));
            }
            extraInfo.setFont(Font.font("", FontWeight.BOLD, -1));
            extraInfo.setText("Please select a curve first to enter this mode.");
        }else{
            extraInfo.setText("Please select a curve first to enter this mode.");
            model.setWrongBtnPressedCount(1);
        }

    }
}
