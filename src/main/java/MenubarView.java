import javafx.geometry.Insets;
import javafx.scene.control.Alert;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;

public class MenubarView extends MenuBar implements IView{

    // reference to the model
    private Model model;

    // File menu
    private Menu fileMenu = new Menu("File");
    private MenuItem newMI = new MenuItem("New");
    private MenuItem loadMI = new MenuItem("Load");
    private MenuItem saveMI = new MenuItem("Save");
    private MenuItem quitMI = new MenuItem("Quit");

    // Help menu
    private Menu helpMenu = new Menu("Help");
    private MenuItem aboutMI = new MenuItem("About");

    // some config
    Background c_bg = new Background(new BackgroundFill(Color.BEIGE, CornerRadii.EMPTY, Insets.EMPTY));
    Border c_bd = new Border(new BorderStroke(Color.GRAY, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(1)));

    MenubarView(Model model) {
        // configure the settings of this pane
        this.setBackground(c_bg);
        this.setBorder(c_bd);

        // keep track of the model
        this.model = model;

        // initialize the layout
        // initiate each menu
        fileMenu.getItems().addAll(newMI,loadMI,saveMI,quitMI);
        helpMenu.getItems().addAll(aboutMI);
        // put all menus into the menu bar
        this.getMenus().addAll(fileMenu,helpMenu);

        // register with the model when we're ready to start receiving data
        model.addView(this);

        // set events
        newMI.setOnAction(actionEvent -> {model.newFile(actionEvent);});
        loadMI.setOnAction(actionEvent -> {model.loadFile(actionEvent);});
        saveMI.setOnAction(actionEvent -> {model.saveFile();});
        quitMI.setOnAction(actionEvent -> {model.quitProgram(actionEvent);});

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("About");
        alert.setHeaderText(null);
        alert.setContentText("Author: Kent");

        aboutMI.setOnAction(actionEvent -> {alert.showAndWait();});
    }

    @Override
    public void updateView() {
        // don't really have anything to update
    }
}
