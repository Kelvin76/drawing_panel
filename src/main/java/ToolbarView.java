import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;

import java.util.ArrayList;

public class ToolbarView extends GridPane implements IView{

    // reference to the model
    private Model model;

    // some config
    final private Background c_bg = new Background(new BackgroundFill(Color.AZURE, CornerRadii.EMPTY, Insets.EMPTY));
    final private Border c_bd = new Border(new BorderStroke(Color.DARKGREY, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(1)));

    private static double padding = 5;
    private static double gap = 4;
    private static double maxGap = 4;
    private static double minGap = 1;

    private double buttonMaxHeight = 100;
    private double buttonMaxWidth = 120;
    private double buttonMinHeight = 30;
    private double buttonMinWidth = 40;
    private double buttonHeight = 40;
    private double buttonWidth = 40;

    Image penImg = new Image("penBtn.png", buttonHeight, buttonWidth, true, false);
    Image selectImg = new Image("selectBtn.png", buttonHeight, buttonWidth, true, false);
    Image pointTImg = new Image("angleBtn.png", buttonHeight, buttonWidth, true, false);
    Image eraserImg = new Image("eraserBtn.png", buttonHeight, buttonWidth, true, false);
    Image addPtImg = new Image("addPointBtn.png", buttonHeight, buttonWidth, true, false);
    Image removePtIMG = new Image("removePointBtn.png", buttonHeight, buttonWidth, true, false);


    /* -------- palette components -------- */
    private ToggleButton penTool = new ToggleButton("Pen",new ImageView(penImg));
    private ToggleButton selectionTool = new ToggleButton("Selection",new ImageView(selectImg));
    private ToggleButton pointTypeTool = new ToggleButton("Smooth / Sharp",new ImageView(pointTImg));
    private ToggleButton eraseTool = new ToggleButton("Eraser",new ImageView(eraserImg));
    private ToggleButton addPointTool = new ToggleButton("Add Point",new ImageView(addPtImg));
    private ToggleButton removePointTool = new ToggleButton("Remove Point",new ImageView(removePtIMG));
    private ArrayList<ToggleButton> btnList = new ArrayList<>();
    private final ToggleGroup alignToggleGroup = new ToggleGroup();


    ToolbarView(Model model) {
        // configure the settings of this pane
        this.setBackground(c_bg);
        this.setBorder(c_bd);

        // keep track of the model
        this.model = model;

        // initialize the layout
        this.setPadding(new Insets(padding));
        this.setHgap(gap);
        this.setVgap(gap);

        btnList.add(penTool);
        btnList.add(selectionTool);
        btnList.add(pointTypeTool);
        btnList.add(eraseTool);
        btnList.add(addPointTool);
        btnList.add(removePointTool);

        // modify each button
        for(ToggleButton btn : btnList){
            btn.setLayoutX(padding);
            btn.setLayoutY(padding);
            btn.setMinWidth(buttonMinWidth);
            btn.setMaxWidth(buttonMaxWidth);
            btn.setMinHeight(buttonMinHeight);
            btn.setMaxHeight(buttonMaxHeight);
            // wrap text
            btn.setWrapText(true);
            // add it to the group
            btn.setToggleGroup(alignToggleGroup);
        }

        this.add(penTool, 0, 0);
        this.add(selectionTool, 1, 0);
        this.add(addPointTool, 2, 0);

        this.add(eraseTool, 0, 1);
        this.add(pointTypeTool, 1, 1);
        this.add(removePointTool, 2, 1);

        // set listeners
        this.penTool.setOnAction(actionEvent->{
            model.buttonPressed(Model.BTN.PEN,penTool);
        });
        this.selectionTool.setOnAction(actionEvent->{
            model.buttonPressed(Model.BTN.SELECT,selectionTool);
        });
        this.pointTypeTool.setOnAction(actionEvent->{
            model.buttonPressed(Model.BTN.POINTTYPE,pointTypeTool);
        });
        this.eraseTool.setOnAction(actionEvent->{
            model.buttonPressed(Model.BTN.ERASE,eraseTool);
        });
        this.addPointTool.setOnAction(actionEvent->{
            model.buttonPressed(Model.BTN.ADDPT,addPointTool);
        });
        this.removePointTool.setOnAction(actionEvent->{
            model.buttonPressed(Model.BTN.REMOVPT,removePointTool);
        });

        // register with the model when we're ready to start receiving data
        model.addView(this);
    }

    // set the size of components inside
    public void setLayout(){
        // modify the size
        for(ToggleButton btn : btnList){
            btn.setPrefWidth(buttonWidth);
            btn.setPrefHeight(buttonHeight);
        }
        // if the button is too narrow, do not show text
        if(Math.abs(buttonWidth- buttonMinWidth) < 54){
            for(ToggleButton btn : btnList) {
                btn.setWrapText(false);
            }
        }else{
            for(ToggleButton btn : btnList) {
                btn.setWrapText(true);
            }
        }

        // if no line is selected, three buttons would be disabled

    }

    @Override
    public void updateView() {
        // set the size of the components inside
        gap = (model.getLeftBarWidth() < getHeight())? model.getLeftBarWidth()/80 : getHeight()/80;
        gap = Math.max(gap,minGap);
        gap=Math.min(gap,maxGap);

        setHgap(gap);
        setVgap(gap);
        buttonWidth = model.getLeftBarWidth() / 2 - padding - gap;
        buttonHeight = getHeight() / 2 - padding - gap;
        buttonWidth = Math.min(buttonWidth,buttonMaxWidth);
        buttonHeight = Math.min(buttonHeight,buttonMaxHeight);
        setLayout();

        // get the current mode and decide the button look
        switch (model.getMode()){
            case NA:
                // reset all button display
                for(ToggleButton btn : btnList){
                    btn.setSelected(false);
                }
                break;
            case DRAWING:
            case SELECTING:
            case POINTTYPECHANGING:
            case ERASING:
            case ADDING:
            case REMOVING:
                break;
            default:

        }
    }
}