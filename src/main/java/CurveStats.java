import javafx.scene.Group;

import java.io.Serializable;
import java.util.ArrayList;

// This is for the purpose of saving and loading
public class CurveStats implements Serializable {
    // store all the segments of cubic curves
    public ArrayList<CubicCurveStats> segments = new ArrayList<>();
    // list of points on the curve
    public ArrayList<CoordStats> points = new ArrayList<>();

    // these can be retreived from points
    // public CoordStats StartPoint;
    // public CoordStats EndPoint;
    // private Rectangle bound;

    // Properties of the curve
    public ColorStats color;
    public double thickness;
    public int pattern;

    // store all the data for Curve
    CurveStats(){}

    // deserialize everything back and returns the object back
    public Curve getObject(Model model, Group root){
        return new Curve(model,root,this);
    }
}
