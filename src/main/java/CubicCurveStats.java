import javafx.scene.Group;
import javafx.scene.shape.CubicCurve;

import java.io.Serializable;

// This is for the purpose of saving and loading
public class CubicCurveStats implements Serializable {
    public double StartX;
    public double StartY;
    public double ControlX1;
    public double ControlY1;
    public double ControlX2;
    public double ControlY2;
    public double EndX;
    public double EndY;

    // store all the data for Curve
    CubicCurveStats(CubicCurve curve){
        StartX = curve.getStartX();
        StartY = curve.getStartY();
        ControlX1 = curve.getControlX1();
        ControlY1 = curve.getControlY1();
        ControlX2 = curve.getControlX2();
        ControlY2 = curve.getControlY2();
        EndX = curve.getEndX();
        EndY = curve.getEndY();
    }

    // deserialize everything back and returns the object back
    public CubicCurve getObject(Group root){
        CubicCurve curve = new CubicCurve(StartX,StartY,ControlX1,ControlY1,ControlX2,ControlY2,EndX,EndY);
        root.getChildren().add(curve);
        curve.toFront();
        return curve;
    }
}
