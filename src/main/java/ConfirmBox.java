import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

// this code is partially copied from the example
public class ConfirmBox {
    static boolean answer;

    public static boolean Display(String title, String message) {
        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle(title);
        window.setMinWidth(250);

        Label label = new Label(message);
        label.setAlignment(Pos.CENTER);
        label.setWrapText(true);
        Button yesButton= new Button("Yes");
        yesButton.setOnAction(event -> {
            answer = true;
            window.close();
        });
        Button noButton= new Button("No");
        noButton.setOnAction(event -> {
            answer = false;
            window.close();
        });

        VBox layout = new VBox(10);

        GridPane Btns = new GridPane();

        Btns.add(yesButton, 0, 0);
        Btns.add(noButton, 1, 0);

        Btns.setAlignment(Pos.CENTER);
        Btns.setHgap(20);
        Btns.setVgap(10);

        layout.getChildren().addAll(label, Btns);
        layout.setAlignment(Pos.CENTER);

        Scene scene = new Scene(layout, 150, 100);
        window.setScene(scene);
        window.showAndWait();
        return answer;
    }
}
